<?php

/**
 * @file
 * Contains \Drupal\demo\Form\Multistep\MultistepOneForm.
 */

namespace Drupal\echidna_test_module\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class EventForm.
 *
 * @package Drupal\echidna_test_module\Form\Multistep
 */
class EventForm extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_one';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form_events = $this->thirdPartyEventsData->getEvents();

    $form['event'] = array(
      '#type' => 'select',
      '#title' => $this->t('Event'),
      '#options' => $form_events,
      '#default_value' => $this->store->get('event') ? $this->store->get('event') : 'default',
      '#required' => TRUE,
    );

    $form['actions']['submit']['#value'] = $this->t('Next');

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('event') == 'default') {
      $form_state->setErrorByName('city', $this->t('Please select a event.'));
    }
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('event', $form_state->getValue('event'));
    $form_state->setRedirect('echidna_test_module.multistep_two');
  }

}
