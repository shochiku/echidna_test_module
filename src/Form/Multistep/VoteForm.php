<?php

/**
 * @file
 * Contains \Drupal\demo\Form\Multistep\MultistepTwoForm.
 */

namespace Drupal\echidna_test_module\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\echidna_test_module\EchidnaTestStorage;

/**
 * Class VoteForm.
 *
 * @package Drupal\echidna_test_module\Form\Multistep
 */
class VoteForm extends MultistepFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multistep_form_two';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $event = $this->store->get('event');
    $get_city = $this->thirdPartyEventsData->getCities($event);
    $form['city'] = array(
      '#type' => 'select',
      '#options' => $get_city,
      '#default_value' => 'default',
      '#title' => 'Which city should host ' . $this->store->get('event'),
      '#required' => TRUE,
    );

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#default_value' => $this->currentUser->getAccountName() ? $this->currentUser->getAccountName() : '',
    );

    $form['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#default_value' => $this->currentUser->getEmail() ? $this->currentUser->getEmail() : '',
    );

    $form['actions']['previous'] = array(
      '#type' => 'link',
      '#title' => $this->t('Previous'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#weight' => 0,
      '#url' => Url::fromRoute('echidna_test_module.multistep_one'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('city') == 'default') {
      $form_state->setErrorByName('city', $this->t('Please select a city.'));
    }

    if (empty($form_state->getValue('name'))) {
      $form_state->setErrorByName('name', $this->t('Please enter your name'));
    }

    if (filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL) === FALSE) {
      $form_state->setErrorByName('email', $this->t('Please enter a valid email address.'));
    }

  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $event = $this->store->get('event');
    $city = $form_state->getValue('city');
    $name = $form_state->getValue('name');
    $email = $form_state->getValue('email');

    //todo do I need these?
//    $this->store->set('city', $city);
//    $this->store->set('name', $name);
//    $this->store->set('email', $email);


    EchidnaTestStorage::addVote($event, $name, $email, $city);

    drupal_set_message($this->t("You voted for next year's @a  to take place in @b.",
      array(
        '@a' => $event,
        '@b' => $city,
      )));
    drupal_set_message('Here are the current rankings for ' . $event . ' : ');

    $counted_votes = EchidnaTestStorage::tallyVotes($event);
    $rank = 0;

    foreach ($counted_votes as $counted_vote) {
      $rank++;
      drupal_set_message($rank . '. ' . $counted_vote->vote);
    }


    // Save the data.
    parent::saveData();

    // If I want to create a seperate page:
    // see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Form!FormBuilder.php/function/FormBuilder%3A%3AredirectForm/8
    $form_state->setRedirect('echidna_test_module.multistep_one');
  }

}
