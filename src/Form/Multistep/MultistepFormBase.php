<?php

/**
 * @file
 * Contains \Drupal\demo\Form\Multistep\MultistepFormBase.
 */

namespace Drupal\echidna_test_module\Form\Multistep;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\echidna_test_module\ThirdPartyEventsData;

/**
 * Class MultistepFormBase.
 *
 * An abstract class for building a base form.
 *
 * This class will be in charge of injecting the necessary dependencies,
 * scaffolding the form, processing the end result and anything else that
 * is needed and is common to both.
 *
 * @package Drupal\echidna_test_module\Form\Multistep
 */
abstract class MultistepFormBase extends FormBase {

  public $thirdPartyEventsData;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * A temporary store that is private to the current user(PrivateTempStore).
   *
   * @var \Drupal\user\PrivateTempStore
   *
   * We will keep all the submitted data from the form steps in this store.
   */
  protected $store;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Allows us to check if the current user is anonymous.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;



  /**
   * Constructs a \Drupal\demo\Form\Multistep\MultistepFormBase.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   * @param \Drupal\Core\Session\AccountInterface $current_user
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, AccountInterface $current_user, ThirdPartyEventsData $third_party_events_data) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;

    $this->thirdPartyEventsData = $third_party_events_data;
    $this->store = $this->tempStoreFactory->get('multistep_data');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('session_manager'),
      $container->get('current_user'),
      $container->get('echidna_test_module.events_data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Start a manual session for anonymous users.
    if ($this->currentUser->isAnonymous() && !isset($_SESSION['multistep_form_holds_session'])) {
      $_SESSION['multistep_form_holds_session'] = TRUE;
      $this->sessionManager->start();
    }

    $form = array();
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#weight' => 10,
    );

    return $form;
  }

  /**
   * Saves the data from the multistep form.
   */
  protected function saveData() {
    // Logic for saving data.
    // todo: idea -- instead of deleting the store, use it to test whether user has already voted.
    $this->deleteStore();
    drupal_set_message($this->t('Thank you for voting!'));
  }

  /**
   * Helper method.
   *
   * Removes all the keys from the store collection used for
   * the multistep form.
   */
  protected function deleteStore() {
//    $keys = ['name', 'email', 'event', 'city'];
//    foreach ($keys as $key) {
//      $this->store->delete($key);
//    }
    $this->store->delete('event');
  }

}
