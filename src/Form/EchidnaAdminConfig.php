<?php

/**
 * @file
 * Contains \Drupal\echidna_test_module\Form\EchidnaAdminConfig.
 */

namespace Drupal\echidna_test_module\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class EchidnaAdminConfig.
 *
 * @package Drupal\echidna_test_module\Form
 */
class EchidnaAdminConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'echidna_test_module.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'echidna_admin_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('echidna_test_module.settings');

    // todo: santize.
    $form['current'] = array(
      '#type' => 'markup',
      '#title' => 'Current Rest URL',
      '#markup' => '<b>Current Data Source : </b>' . $config->get('rest_uri'),

    );

    $form['url'] = array(
      '#type' => 'url',
      '#title' => $this->t('Change URL for Event Voting :'),
      '#size' => 90,
    );

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Set'),
      '#button_type' => 'primary',
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


    // todo: if $form_state->getValue('url') isn't json data, set a drupal message; maybe do this in the validation instead, and sanitize
    $url = $form_state->getValue('url');

    // Save form data to config.
    $this->config('echidna_test_module.settings')
      ->set('rest_uri', $url)
      ->save();

    // Delete any outdated cached data from previous source.
    \Drupal::cache()->delete('echidna_events_cache');

    parent::submitForm($form, $form_state);

  }


}
