<?php

namespace Drupal\echidna_test_module;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Class ThirdPartyEventsData.
 *
 * Custom Service for handling 3rd Party REST data.
 *
 * @package Drupal\echidna_test_module
 */
class ThirdPartyEventsData {

  /**
   * Json-decoded events data from an external third party system.
   *
   * @var array
   */
  protected $eventsData;

  /**
   * The cache.default cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * ThirdPartyEventsData constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *    Default cache.
   */
  public function __construct(CacheBackendInterface $cache_backend) {
    $this->cacheBackend = $cache_backend;
    $this->loadEventsData();
  }

  /**
   * Fetches and decodes raw jsonp data from the configured Third Party source.
   */
  public function loadEventsData() {
    // URI for the source data to be used in the form.
    // This URI is configurable via /admin/config/media/echidna.
    $request_uri  = \Drupal::config('echidna_test_module.settings')->get('rest_uri');

    // If the source rest data has already been saved to cache, retrieve it.
    if ($cache = $this->cacheBackend->get('echidna_events_cache')) {
      $this->eventsData = $cache->data;
    }
    // Else, retrieve rest data and cache it.
    else {

      $response = file_get_contents($request_uri);

      // If we did not get a response data, log an error.
      if ($response === FALSE) {
        \Drupal::logger('echidna_test')->error("Could not connect to Third Party Data source");
      }

      // Decode the source rest json/jsonp data.
      $this->eventsData = $this->decode3rdPartyData($response, TRUE);

      // Save the decoded rest data to cache_default.
      $this->cacheBackend->set('echidna_events_cache', $this->eventsData, CacheBackendInterface::CACHE_PERMANENT);
    }
  }

  /**
   * Helper function that decodes 3rd Party Json Data.
   *
   * @param string $jsonp
   * @param bool $assoc
   * @return mixed
   */
  private function decode3rdPartyData($jsonp, $assoc = TRUE) {

    // If data is in JSONP format, strip out the padding.
    if ($jsonp[0] !== '[' && $jsonp[0] !== '{') {
      $jsonp = substr($jsonp, strpos($jsonp, '('));
    }
    return json_decode(trim($jsonp, '();'), $assoc);
  }

  /**
   * Generates an array of Events.
   *
   * @return array
   *    Array has been formatted to render in form.
   */
  public function getEvents() {
    $events_list = array('default' => 'choose an event');
    foreach ($this->eventsData as $event) {
      $events_list[$event['name']] = $event['name'];
    }
    return $events_list;
  }

  /**
   * Generates an array of Cities, based on chosen event.
   *
   * @return array
   *    Array has been formatted to render in form.
   */
  public function getCities($chosen_event = '') {
    $cities = array('default' => 'choose a city');
    foreach ($this->eventsData as $event) {
      if ($event['name'] == $chosen_event) {
        foreach ($event['cities'] as $city) {
          $cities[$city] = $city;
        }
      }
    }
    return $cities;

  }

}
