<?php

namespace Drupal\echidna_test_module;

/**
 * Class EchidnaTestStorage.
 *
 * @package Drupal\echidna_test_module
 *
 * Provides methods for adding and retrieving data from module's database table
 * (echidna_test_module_votes).
 */
class EchidnaTestStorage {

  /**
   * Todo: use this static variable for table instead of hardcoding.
   */
  public static $table = 'echidna_test_module_votes';

  /**
   * Inserts a vote record into the echidna_test_module_vote table.
   *
   * @param string $event
   * @param string $name
   * @param string $email
   * @param string $vote
   */
  public static function addVote($event, $name, $email, $vote) {
    db_insert('echidna_test_module_votes')
      ->fields(array(
        'event' => $event,
        'name' => $name,
        'email' => $email,
        'vote' => $vote,
      ))->execute();
  }

  /**
   * Counts the votes for each candidate city for an event.
   *
   * Data for each event is cached, if necessary.
   *
   * @param string $event
   *    The event to count votes for.
   *
   * @return mixed
   *    Array containing vote count for each city.
   */
  public static function tallyVotes($event) {
    // CacheId for a specific event.
    $cache_id = 'echidna_event_' . $event;

    // Remove expired cache data.
    \Drupal::cache()->garbageCollection();

    // If a valid copy of data is already cached, return it.
    if ($cache = \Drupal::cache()->get($cache_id, TRUE)) {
      // $tmp = "from cache";
      return $cache->data;
    }

    // Else, count, cache, and return the vote data.
    else {
      // Count the votes for selected event.
      $data = self::countVotes($event);

      // Todo: could make this configurable.
      $expiry = time() + 60;

      // Store vote count data to cache.
      \Drupal::cache()->set($cache_id, $data, $expiry);

      // Return the vote count data.
      return $data;
    }
  }

  /**
   * Queries the DB and counts the votes for each candidate city.
   *
   * @param string $event
   *    The event to count votes for.
   *
   * @return mixed
   *    Object containing vote count information for each city
   */
  public static function countVotes($event) {
    $result = db_query('SELECT vote, COUNT(*) as \'Cnt\' FROM {echidna_test_module_votes} WHERE event = :event GROUP BY vote ORDER BY Cnt DESC', array(':event' => $event));
    return $result->fetchAll();
  }

}
